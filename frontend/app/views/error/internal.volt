{# Internal Server Error View #}

<h1 class="text-center">{{ trans._("¡Algo ha ocurrido!") }}</h1>

{% if error_message is defined %}
	<p>{{ error_message|nl2br }}</p>
{% else %}
	<p>{{ trans._("Ha surgido un problema inesperado, estaremos solucionando esto a la brevedad.") }}</p>
{% endif %}

{% if go_back is defined %}
	<div class="text-center">
		<a href="{{ url(go_back) }}">{{ trans._("Volver atrás") }}</a>
	</div>
{% endif %}
