{# Index Page #}
<section class="dashboard">
        <div class="action" id="tactic">
            <a href="{{ url('course/tactic/index') }}"><h1>T&aacute;ctica</h1>
                <div class="score float-left">
                    <ul>
                        <li class="star-on float-left"></li>
                        <li class="star-off float-left"></li>
                        <li class="star-off float-left"></li>
                        <li class="star-off float-left"></li>
                        <li class="star-off float-left"></li>
                    </ul>
                </div>
                <div class="percentage float-right">
                    <span class="percent-symbol">%</span>
                    <span class="count">13</span>
                </div>
            </a>
        </div>
        <div class="action" id="technique">
            <a href="{{ url('course/technic/index') }}"><h1>T&eacute;cnica</h1>
                <div class="score float-left">
                    <ul>
                        <li class="star-on float-left"></li>
                        <li class="star-on float-left"></li>
                        <li class="star-on float-left"></li>
                        <li class="star-off float-left"></li>
                        <li class="star-off float-left"></li>
                    </ul>
                </div>
                <div class="percentage float-right">
                    <span class="percent-symbol">%</span>
                    <span class="count">43</span>
                </div>
            </a>
        </div>
</section>
<section class="dashboard">
    <div class="action" id="defense">
        <a href="{{ url('course/defense/index') }}"><h1>Defensa</h1>
            <div class="score float-left">
                <ul>
                    <li class="star-on float-left"></li>
                    <li class="star-on float-left"></li>
                    <li class="star-off float-left"></li>
                    <li class="star-off float-left"></li>
                    <li class="star-off float-left"></li>
                </ul>
            </div>
            <div class="percentage float-right">
                <span class="percent-symbol">%</span>
                <span class="count">28</span>
            </div>
        </a>
    </div>
    <div class="action" id="attack">
        <a href="{{ url('course/attack/index') }}"><h1>Ataque</h1>
            <div class="score float-left">
                <ul>
                    <li class="star-on float-left"></li>
                    <li class="star-on float-left"></li>
                    <li class="star-on float-left"></li>
                    <li class="star-on float-left"></li>
                    <li class="star-off float-left"></li>
                </ul>
            </div>
            <div class="percentage float-right">
                <span class="percent-symbol">%</span>
                <span class="count">71</span>
            </div>
        </a>
    </div>
</section>