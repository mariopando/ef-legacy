<section class="player">
    <section class="breadcrumb">
        <a href="{{ url() }}"><h3>» ciclos formativos .ataque.CONSTRUCCIÓN EN ATAQUE</h3></a> <h3><span class="blue">.LATERAL EN ATAQUE</span></h3>
    </section>
    <div class="player-header">
        <h2>Lateral en Defensa</h2>
        <a class="next-courselink" href="{{ url('course/defense/lateral') }}">siguiente curso</a>
    </div>
    <div class="player-body">
        <div class="column player-main">
            <ul class="timeline">
                <li class="active" data-seek-second="5">Jugada de ataque construido, desborde  y pase centro. La jugada se inicia en un balón  que captura el volante defensivo en tareas de relevo de su compañero.</li>
                <li data-seek-second="15" >En esta jugada , existen factores que llevan al éxito esta ocasión creada. Primero , destacar el destacar el destacar el destacar el destacar el  </li>
                <li data-seek-second="20" >Su visión periférica en la conducción de balón, su noción de espacios y tiempos en el juego, ponen a ponen a ponen a ponen a ponen a ponen a</li>
                <li data-seek-second="30" >Este pase que entrega el volante creativo lo realiza de manera que el balón pasa entre dos rivales dos rivales dos rivales dos rivales dos rivales</li>
                <li data-seek-second="40" >Corre en forma diagonal en dirección al área, a la espalda del lateral izquierdo rival que no alcanzaría no alcanzaría no alcanzaría no alcanzaría</li>
                <li data-seek-second="50" >Al momento de recibir el pase, ejecuta de primera. A un toque, un pase rasante y muy fuerte debido fuerte debido fuerte debido fuerte debido</li>
                <li data-seek-second="60" > Frontal al arco con un marcador pegado a él. Este atacante realiza un freno y acelera nuevamente pero nuevamente pero nuevamente pero nuevamente pero</li>
            </ul>
            <div class="player-control">
                <img id="back" class="float-left" src="{{ static_url('images/icons/back-icon.png') }}" />
                <img id="pause" class="float-left hide" src="{{ static_url('images/icons/pause-on-icon.png') }}" />
                <div id="play" class="float-left"></div>
                {#<img id="stop" class="float-left" src="{{ static_url('images/icons/stop-icon.png') }}" />#}
                <img id="forward" class="float-left" src="{{ static_url('images/icons/forward-icon.png') }}" />
                <img id="fullscreen" class="float-right" src="{{ static_url('images/icons/fullscreen-icon.png') }}" />
            </div>
        </div>
        <div class="video-box" id="video_box"></div>
    </div>
</section>
