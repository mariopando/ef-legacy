<section class="subcourses">
    <section class="breadcrumb">
        <a href="{{ url() }}"><h3>» ciclos formativos</h3></a> <h3><span class="blue"> .T&Eacute;CNICA</h3></span>
    </section>
    <a href="{{ url('course/technic/construction') }}">
        <div class="subcourse">
            <div class="pic shine-effect">
                <img src="{{ static_url('images/bgs/attack-bg1.jpg') }}" />
                <div class="percentage">
                    <span class="font-size-47">23</span>%
                </div>
            </div>
            <div class="description-box">
                <div class="large-11 description columns">
                    <h1>Construcci&oacute;n en T&eacute;cnica</h1>
                    <ul class="subcourse-indicator">
                        <li><img src="{{ static_url('images/icons/icon_shot.png') }}"/></li>
                        <li><img src="{{ static_url('images/icons/icon_tackling.png') }}"/></li>
                        <li><img src="{{ static_url('images/icons/icon_arc.png') }}"/></li>
                        <li><img src="{{ static_url('images/icons/icon_cup.png') }}"/></li>
                        <li><img src="{{ static_url('images/icons/icon_flag.png') }}"/></li>
                    </ul>
                    <h2>10 Videos, 3 pruebas</h2>
                </div>
                <div class="large-1 subcourse-name columns">
                    <h1 id="technic-vertical-text">T&eacute;cnica</h1>
                </div>
            </div>
        </div>
    </a>

    <div class="inactive subcourse">
        <div class="pic">
            <img src="{{ static_url('/images/bgs/attack-bg2.jpg') }}" />
        </div>
        <div class="description-box">
            <div class="large-11 description columns">
                <h1>Contrataque</h1>
                <ul class="subcourse-indicator">
                    <li><img src="{{ static_url('images/icons/icon_shot.png') }}"/></li>
                    <li><img src="{{ static_url('images/icons/icon_tackling.png') }}"/></li>
                    <li><img src="{{ static_url('images/icons/icon_arc.png') }}"/></li>
                </ul>
                <h2>10 Videos, 3 pruebas</h2>
            </div>
            <div class="large-1 subcourse-name columns">
                <h1 id="technic-vertical-text">T&eacute;cnica</h1>
            </div>
        </div>
    </div>

    <div class="inactive subcourse">
        <div class="pic">
            <img src="{{ static_url('images/bgs/attack-bg3.jpg') }}" />
        </div>
        <div class="description-box">
            <div class="large-11 description columns">
                <h1>T&aacute;ctica fija en T&eacute;cnica</h1>
                <ul class="subcourse-indicator">
                    <li><img src="{{ static_url('images/icons/icon_shot.png') }}"/></li>
                    <li><img src="{{ static_url('images/icons/icon_tackling.png') }}"/></li>
                    <li><img src="{{ static_url('images/icons/icon_arc.png') }}"/></li>
                </ul>
                <h2>10 Videos, 3 pruebas</h2>
            </div>
            <div class="large-1 subcourse-name columns">
                <h1 id="technic-vertical-text">T&eacute;cnica</h1>
            </div>
        </div>
    </div>
</section>