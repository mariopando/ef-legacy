<section class="content">
    <section class="breadcrumb">
        <a href="{{ url() }}"><h3>» ciclos formativos .ataque</h3></a> <h3><span class="blue">.CONSTRUCCIÓN EN ATAQUE</h3></span>
    </section>
    <div class="content-header">
        <div class="columns large-10">
            <h2>Construcción en Ataque</h2>
            <ul class="score">
                <li class="star-full"></li>
                <li class="star-full"></li>
                <li class="star-full"></li>
                <li class="star-half"></li>
                <li class="star-empty"></li>
                <li class="score-legend"> [15k]</li>
            </ul>

            <ul class="circles">
                <li><img src="{{ static_url('images/icons/icon_shot.png') }}"/></li>
                <li><img src="{{ static_url('images/icons/icon_tackling.png') }}"/></li>
                <li><img src="{{ static_url('images/icons/icon_arc.png') }}"/></li>
                <li><img src="{{ static_url('images/icons/icon_cup.png') }}"/></li>
                <li><img src="{{ static_url('images/icons/icon_flag.png') }}"/></li>
            </ul>
        </div>
    </div>
    <div class="content-body">
        <h2>10 Videos, 3 Pruebas</h2>
        <p>En el fútbol específicamente,  atacar se refiere a toda acción ejecutada con el balón que tenga como intención, y propósito final el gol. Dentro de Cada acción con balón, dependiendo de cada zona del terreno de juego.. <a href="#">VER MÁS</a></p>
        <div class="content-body-types">
            <div class="large-3 float-left">
                <p><a href="{{ url('course/attack/lateral') }}">1. LATERAL EN ATAQUE</a></p>
                <p><a href="#">2. DESBORDE EN ATAQUE</a></p>
                <p><a href="#">3. JUGADA EN AMPLITUD</a></p>
            </div>
            <div class="large-3 float-left">
                <p><a href="#">4. LÍNEA DE FONDO Y CENT..</a></p>
                <p><a href="#">5. PENETRACIÓN CON DOS..</a></p>
                <p class="test-link"><a href="{{ url('exam/attack/attack_construction') }}">HAZ LA PRUEBA</a></p>
            </div>
        </div>
    </div>
</section>
