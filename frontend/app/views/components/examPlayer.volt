{#Exam Player Component#}
<script type="text/html" id="vue-template-exam-player">
    <div class="player-main" v-if="questions">
        <ul class="timeline exam">
            <li v-for="question in questions" v-text="question.longText" v-on:click="setActive($index)" v-bind:class="[active]"></li>
        </ul>
    </div>
    <div class="exam-content exam" v-if="answers">
        <ul class="questions" data-answers-id="" v-for="answersGroup in answers">
            <li v-for="answer in answersGroup.data" v-text="answer.longText"></li>
        </ul>
    </div>
</script>