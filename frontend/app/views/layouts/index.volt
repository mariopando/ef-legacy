{#  Index Parent Layout
	==============================================
#}


{# Index Wrapper #}
<div class="app-index-wrapper app-container-wrapper">
	{# Header TopBar #}
	{{ partial("templates/header") }}
	{# content #}
	{{ get_content() }}
</div>
