{#  Errors Parent Layout
	==============================================
#}

{# Error Wrapper #}
<div class="app-errors-wrapper">

	{# logo #}
	<div class="text-center">

		{# top space for legacy browsers #}
		{% if client.isLegacy %}
			<div style="margin-top:150px;">&nbsp;</div>
		{% endif %}

		{#<a href="{{ url() }}">
			<img class="logo" src="{{ static_url('images/logos/logo-ls-light.png') }}" data-retina alt="" />
		</a>#}
	</div>

    {# content #}
    <div class="error-content text-center">
		{{ get_content() }}
	</div>

</div>
