{#  Password Recovery Parent Layout
	==============================================
#}

{# Auth Wrapper #}
<div class="app-auth-wrapper">
	{# SubHeader account #}
	{{ partial("templates/headerForm") }}
    {# content #}
	{{ get_content() }}
</div>
