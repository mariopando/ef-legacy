{#  Account Parent Layout
	==============================================
#}

{# Account Wrapper #}
<div class="app-account-wrapper app-container-wrapper">
    {# Header TopBar #}
    {{ partial("templates/header") }}
    {# content #}
    {{ get_content() }}
</div>
