{#  Contents Parent Layout
	==============================================
#}

{# Header TopBar #}
{{ partial("templates/header") }}

{# Contents Wrapper #}
<div class="app-contents-wrapper app-container-wrapper">
    {{ get_content() }}
</div>
