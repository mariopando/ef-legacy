{#  Auth Parent Layout
	==============================================
#}

{# Auth Wrapper #}
<div class="app-auth-wrapper">
	{# SubHeader account (logo) #}
	{{ partial("templates/headerForm") }}
	{# content #}
	{{ get_content() }}
</div>
