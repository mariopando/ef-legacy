{# HEADER #}
<header>
    <nav class="top-bar">
        <div class="top-bar-left">
            <a href="{{ url() }}">&nbsp;</a>
        </div>

        <div class="top-bar-right">
            <ul class="top-navigation menu">
                <li class="active"><a href="#">academia</a></li>
                <li><a href="#">comunidad</a></li>
            </ul>
            <ul class="dropdown menu profile" data-dropdown-menu>
                <li class="float-right"><img class="profile-photo" src="{{ static_url('images/icons/icon_photouser.png') }}"/></li>
                <li class="float-right">
                    <a href="#" class="profile-name">
                        <img class="flag" src="{{ static_url('images/icons/flag.png') }}"/>
                        Ignacio Pérez</a>
                        <ul class="menu vertical" style="visibility:hidden;">
                            {#<li><a href="#"><img src="{{ static_url('images/icons/icon_ball.png') }}"/></a>Progress</li>#}
                            {#<li><a href="#"><img src="{{ static_url('images/icons/icon_user.png') }}"/></a>Profile</li>#}
                            {#<li><a href="#"><img src="{{ static_url('images/icons/settings.png') }}"/><span class="alert badge float-left">3</span>Settings</a></li>#}
                            {#<li><a href="{{ url('auth/logout') }}"><img src="{{ static_url('images/icons/log_out.png') }}"/>Logout</a></li>#}
                            {#<li><a href="#">Progress</a></li>#}
                            {#<li><a href="#">Profile</a></li>#}
                            {#<li><a href="#">Settings</a></li>#}
                            <li><a href="{{ url('auth/logout') }}">Logout</a></li>
                        </ul>
                </li>
                <li class="float-right">
                    <a href="#" class="alert round"><i class="icon-alert"></i>7 días de prueba</a>
                </li>
            </ul>
        </div>
    </nav>
</header>
