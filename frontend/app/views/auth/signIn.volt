{# SignIn View #}
<section id="vue-auth" class="signin-box">

    <div class="signin-header row">
        <div class="column small-12">
            <img src="{{ static_url('images/logos/logo-large.png') }}" />
        </div>
    </div>
    <div class="signin-body row small-collapse">
        <div class="column small-12">
            <button class="app-btn-fb styled" data-action="login" disabled="disabled" data-fb-loaded="{{ trans._('Inicia con Facebook') }}">
                <span>{{ trans._('Cargando facebook...') }}</span>
                {#<img src="{{ static_url('images/icons/facebook-icon.png') }}" data-retina alt="" />#}
            </button>
        </div>
    </div>
    <div class="signin-body row">
        <div class="column small-12 app-form">
            <form data-validate v-on:submit="loginUserByEmail" action="javascript:void(0);">
                <p class="text-center">o</p>

                <div class="row small-collapse">
                    <div class="column small-12">
                        <input name="email" type="email" size="50" maxlength="255" v-model="loginInputEmail"
                               autocomplete="on" placeholder="{{ trans._('Correo electrónico') }}"
                               data-fv-required data-fv-message="{{ trans._('Ingresa un correo electrónico válido.') }}" />
                    </div>
                </div>

                <div class="row small-collapse">
                    <div class="column small-12">
                        <input name="pass" type="password" size="50" maxlength="20" autocomplete="off" placeholder="{{ trans._('Contraseña') }}"
                               data-fv-required data-fv-message="{{ trans._('Ingresa tu contraseña.') }}" />
                    </div>
                </div>

                <div class="row small-collapse">
                    <div class="column small-12 text-right">
                        <button type="submit" class="submit">{{ trans._('Iniciar Sesión') }}</button>
                    </div>
                </div>

                {#<div class="control align-justify">
                    <div class="float-left">
                        <input id="checkbox1" type="checkbox"><label for="checkbox1">Recordarme</label>
                    </div>
                    <div class="float-right">
                    </div>
                </div>#}
            </form>
        </div>
    </div>
    <div class="signin-footer row">
        <div>
            <p><a href="#">{{ trans._('¿No puedes entrar en tu cuenta?') }}</a> <a href="#">{{ trans._("¿Olvidaste tu contraseña?") }}</a></p>
        </div>
        <div>
            <p>{{ trans._('¿No tienes una cuenta?') }} <a href="#">{{ trans._('¡Regístrate!') }}</a></p>
        </div>
    </div>

</section>
