<?php
/**
 * Phalcon App Routes files
 * 404 error page is managed by Route 404 Plugin automatically
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

return function($router) {

	//default route
	$router->add("/", [
	    'controller' => 'auth',
	    'action'     => 'signIn'
	]);

	//login & register
	$router->add("/signIn", [
	    'controller' => 'auth',
	    'action'     => 'signIn'
	]);

	$router->add("/signUp", [
	    'controller' => 'auth',
	    'action'     => 'signUp'
	]);

	//events
	$router->add("/event/:params", [
	    'controller' => 'events',
	    'action'     => 'index',
	    'params'	 => 1
	]);

	//EF courses
	$router->add("/course/:namespace/:action", [
		'controller' => 'course',
		'action'     => 'index',
		'_namespace'  => 1,
		'_section'  => 2
	]);

	//EF tests
	$router->add("/exam/:namespace/:action", [
		'controller' => 'exam',
		'action'     => 'index',
		'_namespace' => 1,
		'_section'   => 2
	]);
};
