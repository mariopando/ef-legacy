<?php
/**
 * Course Controller, handles courses pages
 * @author Mario Pando <mario.pando@crazycake.cl>
 */

use Vimeo\Vimeo;

class CourseController extends SessionController {

    public function indexAction($namespace = null, $section = null)
    {
        //NOTE: hardcoded
        if($section == 'lateral') {

            try{
                // vimeo instance
                $lib = new Vimeo($this->config->app->vimeo->clientID,
                                 $this->config->app->vimeo->clientSecret,
                                 $this->config->app->vimeo->accessToken);

                //searching
                $all_search = $lib->request('/me/videos');

                //s($all_search); die;

                //compounding output data
                foreach($all_search['body']['data'] as $video):
                    $videos[] = [
                        'vimeo_embed_html' => $video['embed']['html'],
                        'vimeo_sd_url'     => $this->returnValueByUniqueKeyComparison( 'quality', 'sd', 'link_secure', $video['files']),
                        'vimeo_hls_url'    => $this->returnValueByUniqueKeyComparison( 'quality', 'hls', 'link_secure', $video['files'])
                    ];
                endforeach;
            }
            catch(Vimeo\Exceptions\VimeoRequestException $e){
                s($e);
            }
        }

        //NOTE: OJO con esto!
        $this->view->pick("course/$namespace/$section");

        //load modules
        $this->_loadJsModules([
            "playerLoader" => [
                'player' => 'video',
                'data'   => isset($videos) ? $videos : null
            ]
        ]);
    }

    /**
     * Trait function after render view
     */
    protected function beforeRenderSignUpView()
    {
        //facebook url
        (new FacebookController())->loadFacebookLoginURL();

        //load modules
        $this->_loadJsModules([
            "auth"      => null,
            "forms"     => null,
            "facebook"  => null
        ]);
    }

    /**
     * Search into array by criteria key and return a specific value
     *
     * @param $key_criteria
     * @param $value
     * @param $key_return
     * @param $array
     * @return null
     */
    private function returnValueByUniqueKeyComparison($key_criteria, $value, $key_return, $array)
    {
        foreach ($array as $key => $val) {

            if ($val[$key_criteria] == $value) {
                return $val[$key_return];
            }
        }

        return null;
    }
}
