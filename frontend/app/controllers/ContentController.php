<?php
/**
 * ContentsController: handles Content pages
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

class ContentController extends SessionController
{
    /**
     * View - Terms
     */
    public function termsAction()
    {
        //...
    }

    /**
     * View - Help
     */
    public function faqAction()
    {
        //...
    }

    /**
    * View - Contact page
    */
   public function contactAction()
   {
       //load js modules
       $this->_loadJsModules([
           "contents" => null
       ]);
   }
}
