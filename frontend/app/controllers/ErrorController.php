<?php
/**
 * Error Controllers : all page errors are routed to this controller.
 * Actions cames from traits (notFoundAction, internalAction, oldBrowserAction, expiredAction. etc.)
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

use CrazyCake\Core\WebErrors;

class ErrorController extends CoreController
{
    /* traits */
    use WebErrors;
}
