<?php
/**
 * Exam Controller, handles exams pages
 * @author Mario Pando <mario.pando@crazycake.cl>
 */

class ExamController extends SessionController {

    public function indexAction($namespace = null, $section = null)
    {
        $this->view->pick("exam/$namespace/$section");

        //load modules
        $this->_loadJsModules([
            "playerLoader" => [
                'player' => 'exam',
                'data'   => null
            ]
        ]);
    }

    /**
     * Trait function after render view
     */
    protected function beforeRenderSignUpView()
    {
        //facebook url
        (new FacebookController())->loadFacebookLoginURL();

        //load modules
        $this->_loadJsModules([
            "auth"      => null,
            "forms"     => null,
            "facebook"  => null
        ]);
    }
}
