<?php
/**
 * FacebookController: handles login and facebook actions
 * @todo Build a trait from this controller
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//CrazyCake Traits
use CrazyCake\Facebook\FacebookAuth;

class FacebookController extends SessionController
{
    /* traits */
    use FacebookAuth;

    /**
     * Construct Event
     */
    protected function onConstruct()
    {
        //always call parent constructor
        parent::onConstruct();
        //set configurations
        $this->initFacebookAuth([
            //texts
            "trans" => [
                //texts
                "session_error"    => $this->trans->_('Ocurrió un problema con tu sesión de Facebook, porfavor inténtalo nuevamente. Si aún se presenta este problema, prueba iniciando una nueva sesión en Facebook.'),
                "invalid_email"    => $this->trans->_('No hemos logrado obtener tu correo primario de Facebook, puede que tu correo no esté validado en tu cuenta de Facebook.
                                                            Haz %a_open%click aquí%a_close% para configurar tu correo.', ["a_open" => '<a href="https://www.facebook.com/settings?tab=account&section=email&view" target="_blank">', "a_close" => "</a>"]),
                "oauth_redirected" => $this->trans->_('Ocurrió un problema con tu sesión de Facebook, porfavor inténtalo nuevamente.'),
                "oauth_perms"      => $this->trans->_('Debes aceptar los permisos de la aplicación en tu cuenta de Facebook.'),
                "session_switched" => $this->trans->_('Es posible que tengas abierta otra sesión de Facebook, intenta cerrando tu sesión actual de Facebook.'),
                "account_switched" => $this->trans->_('Esta sesión de Facebook está vinculada a otra cuenta %app_name%, intenta con otra cuenta en Facebook.', ["app_name" => $this->config->app->name]),
                "account_disabled" => $this->trans->_('Esta cuenta se encuentra desactivada por incumplimiento a nuestros términos y condiciones, porfavor %a_open%comunícate aquí%a_close% con nuestro equipo.')
            ]
        ]);
    }

    /* --------------------------------------------------- § -------------------------------------------------------- */

    /**
     * Listener - This method is invoked when user is logged in after redirection strategy
     * @param string $handler_uri - The handler uri
     */
    protected function onSuccessAuth(&$route, $response = null)
    {
        //...
    }

    /**
     * On App Deauthorize
     * @param  object $fb_user - The user facebook ORM object
     */
    protected function onAppDeauthorized($data = array())
    {

    }
}
