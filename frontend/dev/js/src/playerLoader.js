/** ---------------------------------------------------------------------------------------------------------------
 UI Module
 ---------------------------------------------------------------------------------------------------------------- **/
module.exports = function(){
    //++ Module
    var self        = this;
    self.moduleName = "playerLoader";

    //++ Properties

    /**
     * Init
     */
    self.init = function(data){
        // if player is video
        if(data.player && data.player == 'video'){
            self.videoData = data.data;

            switch (self.checkBrowserSupport()){
                case 'mpeg':
                    core.modules.mpegPlayer.init();
                    break;

                case 'vimeo':
                    core.modules.vimeoPlayer.init();
                    break;

                case 'hls_stream':
                    core.modules.adaptativeHlsPlayer.init();
                    break;

                case 'dash_stream':
                    core.modules.adaptativeDashPlayer.init();
            }
            console.log('video!')
        }

        // if player is video
        if(data.player && data.player == 'exam'){
            core.modules.examPlayer.init();
            console.log('exam!')
        }

    };

    /**
     * Check that video player is convenient to use
     * @returns {*}
     */
    self.checkBrowserSupport = function(){
        //if($browser == "MSIE"    && $version < 11) return false;
        //if($browser == "Chrome"  && $version < 18) return false;
        //if($browser == "Firefox" && $version < 14) return false;
        if(UA.browser == "Opera"   && UA.shortVersion < 25) return false; //new rule
        //if($browser == "Safari"  && $version < 5) return false;

        return 'hls_stream';
    };
};