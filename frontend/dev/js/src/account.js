/**
 * Account View Model
 * @class Account
 */
module.exports = function() {

    //++ Module
    var self        = this;
    self.moduleName = "account";

    //++ View Model
    self.vm = {
        methods : {}
    };

    //++ UI Selectors
    _.assign(APP.UI, {
        sel_app_user_name : ".app-user-name"
    });

    //++ Properties
    self.headerNameProfile = $(APP.UI.sel_app_user_name); //get user name text

    //++ Methods

    /**
     * Update profile
     * @param  {object} e - The event handler
     */
    self.vm.methods.updateProfile = function(e) {

        //ajax request
        core.ajaxRequest({ method : 'POST', url :  APP.baseUrl + 'account/updateProfile' }, e.target)
        .then(function(payload) {

            if(!payload) return;

            //check name changed
            if(typeof payload.user.name != "undefined")
                self.headerNameProfile.text(payload.user.name);

            //check pass flag changed
            if(typeof payload.user.pass != "undefined")
                $(e.target).find('input:password').val('');

            //show succes message & scroll to top
            core.showAlert(payload.msg, "success");

        }).done();
    };
};
