/** ---------------------------------------------------------------------------------------------------------------
    UI Module
---------------------------------------------------------------------------------------------------------------- **/
module.exports = function() {

    //++ Module
    var self        = this;
    self.moduleName = "ui";

    //++ UI selectors
    _.assign(APP.UI, {
        //settings
        alert : {
            position    : "fixed",
            top         : "belowHeader",
            topForSmall : "0"
        },
        loading : {
            position    : "fixed",
            top         : "14%",
            topForSmall : "20%",
            center      : true
        },
        //selectors
        sel_header_menu : "#app-header-menu"
    });

    //++ Methods

    /**
     * Init function
     */
    self.init = function() {

        //app load UI
        self.uiSetUp();
    };

    /**
     * App Load Custom Contents Scroll, make exceptions to small screens
     */
    self.uiSetUp = function() {

        $('.count').each(function () {
            $(this).prop('Counter',0).animate({
                Counter: $(this).text()
            }, {
                duration: 4000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
    };
};
