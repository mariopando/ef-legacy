/** ---------------------------------------------------------------------------------------------------------------
 UI Module
 ---------------------------------------------------------------------------------------------------------------- **/
module.exports = function() {
    //++ Module
    var self = this;
    self.moduleName = "vimeoPlayer";

    //++ Properties
    self.player            = $('iframe');
    self.playerOrigin      = '*';
    self.listCollection    = [];
    self.buttonsRef = $('div.player-control');

    //++ Methods

    /**
     * Init
     */
    self.init = function(){
        // Handle messages received from the player
        var onMessageReceived = function(event) {
            // Handle messages from the vimeo player only
            if (!(/^https?:\/\/player.vimeo.com/).test(event.origin)) {
                return false;
            }

            if (self.playerOrigin === '*') {
                self.playerOrigin = event.origin;
            }

            var data = JSON.parse(event.data);

            switch (data.event) {
                case 'ready':
                    self.onReady();
                    break;

                case 'playProgress':
                    self.onPlayProgress(data.data);
                    break;

                case 'pause':
                    self.onPause();
                    break;

                case 'finish':
                    self.onFinish();
                    break;
            }
        };

        // Listen for messages from the player
        if (window.addEventListener) {
            //New browsers
            window.addEventListener('message', onMessageReceived, false);
        }
        else {
            //Old browsers
            window.attachEvent('onmessage', onMessageReceived, false);
        }

        $.when(self.buildListCollection()).done(function(){
            self.bindPlayerButtons();
            self.bindListLinks();
        });
    };

    /**
     * Bind the buttons in DOM
     */
    self.bindPlayerButtons = function(){
        $('#pause, #play', self.buttonsRef).click(function(){
            var button_ref = $(this);
            if(button_ref.is(':visible')){
                button_ref.hide();
                switch(button_ref.attr('id')){
                    case 'play':
                        $('#pause', self.buttonsRef).removeClass('hide').show();
                        self.postAction('play', self.onPlayProgress());
                        break;
                    case 'pause':
                        $('#play', self.buttonsRef).removeClass('hide').show();
                        self.postAction('pause', self.onPause());
                        break;
                }
            }
        });

    };

    /**
     * Link the li lists
     */
    self.bindListLinks = function(){
        $('ul.timeline li').on('click', function() {
            $('ul.timeline li').removeClass('active');
            _self = $(this); // li reference
            _self.addClass('active');

            if(_self.attr('data-seek-second')){
                self.postAction('seekTo', _self.attr('data-seek-second'));
            }
        });
    };

    /**
     *  Make a list collection and assign data seconds label
     */
    self.buildListCollection = function(){
        $('ul.timeline li').each(function(){
            var _self = $(this);

            self.listCollection.push({
                index  : _self.index(), // dom index
                second : _self.attr('data-seek-second'),
                active : (_self.hasClass('active')) ? true : false
            });

            //format value
            _self.attr('data-label-second', (_self.attr('data-seek-second') > 59) ?
            Math.round(_self.attr('data-seek-second') / 60 ) + "'"
                :
                _self.attr('data-seek-second'));
        });
    };

    /**
     * Send messages to player
     * @param action
     * @param value
     */
    self.postAction = function(action, value) {
        var data = {
            method: action
        };

        if (value) {
            data.value = value;
        }

        var message = JSON.stringify(data);
        self.player[0].contentWindow.postMessage(message, self.playerOrigin);
    };

    self.onReady = function() {
        //self.postAction('addEventListener', 'pause');
        //self.postAction('addEventListener', 'finish');
        self.postAction('addEventListener', 'playProgress');
    };

    self.onPause = function() {
        //console.log('paused');
    };

    self.onFinish = function() {
        //console.log('finished');
    };

    /**
     * Player listener that update the DOM UL List
     */
    self.onPlayProgress = function(data) {
        clearInterval(self.interval);
        self.interval = setInterval(function(){
            self.listCollection.filter(function(object, index){
                if(object.second == Math.round(data.seconds) ){
                    self.currentIndex = index;
                    self.updateList();
                }
            });

            if(self.videoReference.paused){
                $('#play', self.buttonsRef).removeClass('hide').show();
            }
            else{
                $('#pause', self.buttonsRef).removeClass('hide').show();
            }
        }, self.intervalFrecuency);
    };

    /**
     * Update the list index
     */
    self.updateList = function(){
        $('ul.timeline li').removeClass('active');

        $('ul.timeline li').eq(self.currentIndex).addClass('active');

        self.listCollection[self.currentIndex].active = true;
    };
};