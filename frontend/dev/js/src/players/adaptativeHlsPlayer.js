/** ---------------------------------------------------------------------------------------------------------------
 UI Module
 ---------------------------------------------------------------------------------------------------------------- **/
module.exports = function() {
    //++ Module
    var self = this;
    self.moduleName = "adaptativeHlsPlayer";
    self.player = null;
    self.listCollection    = [];
    self.currentIndex      = 0;

    //++ Properties

    //++ Methods

    /**
     * Init
     */
    self.init = function(){

        var video_reference = $('<video />').appendTo($('.video-box'));
        var video = $('<source/>', {
            //src      : core.modules.playerLoader.videoData[1].hls_link, // vimeo hls url (with problems)
            src      : 'http://solutions.brightcove.com/jwhisenant/hls/apple/bipbop/bipbopall.m3u8', // url example
            type     : 'application/x-mpegURL'
        });

        video.appendTo(video_reference);
        self.videoReference = video_reference[0];

        $.when(self.buildListCollection()).done(function(){
            self.bindPlayerButtons();
            self.bindListLinks();
            self.loadPlayer();
        });
    };

    /**
     * Init the player
     */
    self.loadPlayer = function(){
        self.player = videojs(self.videoReference,{
            controls: false
        },function(){
            // Player (this) is initialized and ready.
        });
    };

    /**
     *  Make a list collection and assign data seconds label
     */
    self.buildListCollection = function(){
        $('ul.timeline li').each(function(){
            var _self = $(this);

            self.listCollection.push({
                index  : _self.index(), // dom index
                second : _self.attr('data-seek-second'),
                active : (_self.hasClass('active')) ? true : false
            });

            //format value
            _self.attr('data-label-second', (_self.attr('data-seek-second') > 59) ?
            Math.round(_self.attr('data-seek-second') / 60 ) + "'"
                :
                _self.attr('data-seek-second'));
        });
    };

    /**
     * Bind the buttons in DOM
     */
    self.bindPlayerButtons = function(){
        //Bind MPEG Player Buttons
        $('#pause, #play', self.buttonsRef).click(function(){
            self.videoReference.playbackRate = 1.0;
            clearInterval(self.intervalRewind);

            var button_ref = $(this);
            if(button_ref.is(':visible')){
                button_ref.hide();
                switch(button_ref.attr('id')){
                    case 'play':
                        $('#pause', self.buttonsRef).removeClass('hide').show();
                        self.onPlayProgress();
                        self.videoReference.play();
                        break;
                    case 'pause':
                        $('#play', self.buttonsRef).removeClass('hide').show();
                        self.videoReference.pause();
                        break;
                }
            }
        });

        //forward
        $('#forward', self.buttonsRef).click(function(){
            self.goNextPosition();
        });

        //back
        $('#back', self.buttonsRef).click(function(){
            self.goBackPosition();
        });

        if(self.autoPlay){
            self.videoReference.play();
            $('#pause', self.buttonsRef).removeClass('hide').show();
        }else{
            $('#play', self.buttonsRef).removeClass('hide').show();
        }

    };

    /**
     * Link the li lists
     */
    self.bindListLinks = function(){
        $('ul.timeline li').on('click', function() {
            $('ul.timeline li').removeClass('active');
            _self = $(this); // li reference
            _self.addClass('active');

            if(_self.attr('data-seek-second')){
                self.goTo(_self.attr('data-seek-second'));
            }
        });
    };

    /**
     * Player listener that update the DOM UL List
     */
    self.onPlayProgress = function() {
        clearInterval(self.interval);
        self.interval = setInterval(function(){
            self.listCollection.filter(function(object, index){
                if(object.second == Math.round(self.videoReference.currentTime) ){
                    self.currentIndex = index;
                    self.updateList();
                }
            });

            if(self.videoReference.paused){
                $('#play', self.buttonsRef).removeClass('hide').show();
            }
            else{
                $('#pause', self.buttonsRef).removeClass('hide').show();
            }
        }, self.intervalFrecuency);
    };

    /**
     * Move the video and the list to the next position
     */
    self.goNextPosition = function(){
        self.listCollection[self.currentIndex].active = false;

        if( ( self.currentIndex + 1 ) < self.listCollection.length){
            ++self.currentIndex;
            self.updateList();
            var second = self.listCollection[self.currentIndex].second;
            clearInterval(self.interval);

            if( second ){
                self.interval = setInterval(function(){
                    if(Math.round(self.videoReference.currentTime) >= parseInt( second )){
                        self.videoReference.playbackRate = 1.0;
                        clearInterval(self.interval);
                        self.onPlayProgress(); //reanudate play progress listener
                    }
                    else{
                        self.videoReference.playbackRate = 6.0;
                    }
                },self.intervalFrecuency);
            }
        }
    };

    /**
     * Move the video and the list to the previous position
     */
    self.goBackPosition = function(){
        self.listCollection[self.currentIndex].active = false;

        if(( self.currentIndex - 1 ) >= 0){
            --self.currentIndex;
            //update list collection index
            self.updateList();

            var second = self.listCollection[self.currentIndex].second;
            self.goTo(second);

            /** Pseudo effect rewind video **/
            //if(second){
            //    self.interval = setInterval(function(){
            //        self.videoReference.playbackRate = 1.0;
            //        if(Math.round(self.videoReference.currentTime) == parseInt(second)){
            //            clearInterval(self.interval);
            //            self.onPlayProgress(); //reanudate play progress listener
            //        }
            //        else{
            //            self.videoReference.currentTime += -.1;
            //        }
            //    },self.intervalFrecuency);
            //}
        }
    };

    /**
     * Move the video to specific player position
     * @param second
     */
    self.goTo = function(second){
        clearInterval(self.interval);

        self.videoReference.playbackRate = 1.0;
        self.videoReference.currentTime = second;

        self.videoReference.play();

        $('#pause', self.buttonsRef).removeClass('hide').show();
        $('#play', self.buttonsRef).removeClass('hide').hide();
    };

    /**
     * Update the list index
     */
    self.updateList = function(){
        $('ul.timeline li').removeClass('active');

        $('ul.timeline li').eq(self.currentIndex).addClass('active');

        self.listCollection[self.currentIndex].active = true;
    };
};