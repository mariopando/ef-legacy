/** ---------------------------------------------------------------------------------------------------------------
 UI Module
 ---------------------------------------------------------------------------------------------------------------- **/
module.exports = function() {
    //++ Module
    var self = this;
    self.moduleName = "examPlayer";

    //++ Properties
    self.buttonsRef        = null;
    self.questionReference    = null;
    self.listCollection    = [];
    self.autoPlay          = false;
    self.interval          = null;
    self.currentIndex      = 0;
    self.questionTime      = 241; //for each question
    self.currentExamTime   = 241; //for general test

    /**
     * Init
     */
    self.init = function(){
        $.when(self.buildListCollection()).done(function(){
            self.bindListLinks();
            self.bindQuestionListLinks();
            self.makeAwnsers();
        });
    };


    self.makeAwnsers = function(){
        $('ul.questions').eq(0).show(); // show first question
        self.countdownTime(); //Minutes
    };

    /**
     *  Make a list collection and assign data seconds label
     */
    self.buildListCollection = function(){
        $('ul.timeline li').each(function(){
            var _self = $(this);

            self.listCollection.push({
                index  : _self.index(), // dom index
                question_id : _self.attr('data-question-id'),
                active : (_self.hasClass('active')) ? true : false,
                time : self.questionTime // seconds
            });
        });
    };

    /**
     * Linking the li lists
     */
    self.bindListLinks = function(){
        $('ul.timeline li').on('click', function() {
            $('ul.timeline li').removeClass('active');
            _self = $(this); // li reference
            _self.addClass('active');

            self.currentIndex = _self.index();
            clearInterval(self.interval);
            self.countdownTime();

            if(_self.attr('data-question-id')){
                self.goTo(_self.attr('data-question-id'));
            }
        });
    };

    /**
     * Move the question to specific position
     * @param second
     */
    self.goTo = function(data_question_id){
        $('ul.questions').hide();
        $('ul.questions[data-answers-id='+data_question_id+']').show();
    };

    /**
     * Linking the questions list
     */
    self.bindQuestionListLinks = function(){
        $('ul.questions li').on('click', function() {
            _self = $(this); // li reference

            $('li', _self.parent() ).removeClass('active');
            _self.addClass('active');
        });
    };


    /**
     * Update the list index
     */
    self.updateList = function(){
        $('ul.timeline li').removeClass('active');
        $('ul.timeline li').eq(self.currentIndex).addClass('active');

        self.listCollection[self.currentIndex].active = true;
    };

    /**
     * Make a general timer
     */
    self.countdownTime = function() {
        self.interval = setInterval(function(){
            if(self.currentExamTime > 0){
                var current_minutes = Math.floor(self.currentExamTime / 60);
                var seconds = self.currentExamTime % 60;
                self.currentExamTime--;
                $('#counter').html(current_minutes.toString() + ":" + (seconds < 10 ? "0" : "") + String(seconds));
            }else{
                $('#counter').html("0:00");
                clearInterval(self.interval);
            }
        }, 1000);
    };

    /**
     * Make a timer for each question
    */
    self.countdownTimebyEachQuestion = function() {
        self.interval = setInterval(function(){
            if(self.listCollection[self.currentIndex].time > 0){
                var current_minutes = Math.floor(self.listCollection[self.currentIndex].time / 60);
                var seconds = self.listCollection[self.currentIndex].time % 60;
                self.listCollection[self.currentIndex].time--;
                $('#counter').html(current_minutes.toString() + ":" + (seconds < 10 ? "0" : "") + String(seconds));
            }else{
                $('#counter').html("0:00");
                clearInterval(self.interval);
            }
        }, 1000);
    };

};