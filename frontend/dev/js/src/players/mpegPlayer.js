/** ---------------------------------------------------------------------------------------------------------------
 UI Module
 ---------------------------------------------------------------------------------------------------------------- **/
module.exports = function() {
    //++ Module
    var self = this;
    self.moduleName = "mpegPlayer";

    //++ Properties
    self.buttonsRef        = null;
    self.videoReference    = null;
    self.intervalFrecuency = 20;
    self.listCollection    = [];
    self.autoPlay          = true;
    self.interval          = null;
    self.currentIndex      = 0;

    /**
     * Init
     */
    self.init = function(){
        //if video box exists
        if(! $('#video_box').length ){
            return false;
        }
        //video add
        var video_reference = $('<video class="preloaders"></video>').appendTo($('#video_box'));
        var video = $('<source/>', {
            id       : 'video_element',
            class    : 'preloaders',
            preload  : 'auto',
            //src      : core.modules.playerLoader.videoData[0].vimeo_sd_url,
            src      : 'https://player.vimeo.com/external/158228500.sd.mp4?s=9a1eb4223b4ff705d10cdec97ef891240a0f971c&profile_id=164&oauth2_token_id=403289585', // temporal location
            type     : 'video/mp4',
            controls : true
        });

        video.appendTo(video_reference);
        self.videoReference = video_reference[0];

        $.when(self.buildListCollection()).done(function(){
            self.bindPlayerButtons();
            self.bindListLinks();
        });
    };

    /**
     *  Make a list collection and assign data seconds label
     */
    self.buildListCollection = function(){
        $('ul.timeline li').each(function(){
            var _self = $(this);

            self.listCollection.push({
                index  : _self.index(), // dom index
                second : _self.attr('data-seek-second'),
                active : (_self.hasClass('active')) ? true : false
            });

            //format value
            _self.attr('data-label-second', (_self.attr('data-seek-second') > 59) ?
            Math.round(_self.attr('data-seek-second') / 60 ) + "'"
                :
                _self.attr('data-seek-second'));
        });
    };

    /**
     * Bind the buttons in DOM
     */
    self.bindPlayerButtons = function(){
        //Bind MPEG Player Buttons
        $('#pause, #play', self.buttonsRef).click(function(){
            self.videoReference.playbackRate = 1.0;
            clearInterval(self.interval);

            var button_ref = $(this);
            if(button_ref.is(':visible')){
                button_ref.hide();
                switch(button_ref.attr('id')){
                    case 'play':
                        $('#pause', self.buttonsRef).removeClass('hide').show();
                        self.onPlayProgress();
                        self.videoReference.play();
                        break;
                    case 'pause':
                        $('#play', self.buttonsRef).removeClass('hide').show();
                        self.videoReference.pause();
                        break;
                }
            }
        });

        //forward
        $('#forward', self.buttonsRef).click(function(){
            self.goNextPosition();
        });

        //back
        $('#back', self.buttonsRef).click(function(){
            self.goBackPosition();
        });

        if(self.autoPlay){
            self.videoReference.play();
            $('#play', self.buttonsRef).hide();
            $('#pause', self.buttonsRef).removeClass('hide').show();
            self.onPlayProgress(); //initiate play progress listener
        }
    };

    /**
     * Link the li lists
     */
    self.bindListLinks = function(){
        $('ul.timeline li').on('click', function() {
            $('ul.timeline li').removeClass('active');
            _self = $(this); // li reference
            _self.addClass('active');

            if(_self.attr('data-seek-second')){
                self.goTo(_self.attr('data-seek-second'));
                self.onPlayProgress(); //reanudate play progress listener
            }
        });
    };

    /**
     * Move the video and the list to the next position
     */
    self.goNextPosition = function(){
        self.listCollection[self.currentIndex].active = false;

        if( ( self.currentIndex + 1 ) < self.listCollection.length){
            ++self.currentIndex;
            self.updateList();
            var second = self.listCollection[self.currentIndex].second;
            clearInterval(self.interval);

            if( second ){
                self.interval = setInterval(function(){
                    if(Math.round(self.videoReference.currentTime) >= parseInt( second )){
                        self.videoReference.playbackRate = 1.0;
                        clearInterval(self.interval);
                        self.onPlayProgress(); //reanudate play progress listener
                    }
                    else{
                        self.videoReference.playbackRate = 6.0;
                    }
                },self.intervalFrecuency);
            }
        }
    };

    /**
     * Move the video and the list to the previous position
     */
    self.goBackPosition = function(){
        self.listCollection[self.currentIndex].active = false;

        if(( self.currentIndex - 1 ) >= 0){
            --self.currentIndex;
            //update list collection index
            self.updateList();

            // show previous list
            if($('ul.timeline li').eq(self.currentIndex).not(':visible')){
                $('ul.timeline li').eq(self.currentIndex).fadeIn();
            }

            var second = self.listCollection[self.currentIndex].second;
            self.goTo(second);

            /** Pseudo effect rewind video **/
            //if(second){
            //    self.interval = setInterval(function(){
            //        self.videoReference.playbackRate = 1.0;
            //        if(Math.round(self.videoReference.currentTime) == parseInt(second)){
            //            clearInterval(self.interval);
            //            self.onPlayProgress(); //reanudate play progress listener
            //        }
            //        else{
            //            self.videoReference.currentTime += -.1;
            //        }
            //    },self.intervalFrecuency);
            //}
        }
    };

    /**
     * Update the list index
     */
    self.updateList = function(){
        $('ul.timeline li').removeClass('active');

        $('ul.timeline li').eq(self.currentIndex).addClass('active');

        self.listCollection[self.currentIndex].active = true;
    };

    /**
     * Hide the previous playlist
     */
    self.showPrevList = function(){
        for(var e =  0; e < self.currentIndex; e++){
            $('ul.timeline li').eq(e).fadeIn();
        }
    };

    /**
     * Player listener that update the DOM UL List
     */
    self.onPlayProgress = function() {
        clearInterval(self.interval);
        self.interval = setInterval(function(){
            self.listCollection.filter(function(object, index){
                if(object.second == Math.round(self.videoReference.currentTime) ){
                    self.currentIndex = index;
                    self.hidePrevList();//hide effect
                    self.updateList();
                }
            });

            if(self.videoReference.paused){ // if is paused
                $('#play', self.buttonsRef).removeClass('hide').show();
            }
            else{
                $('#pause', self.buttonsRef).removeClass('hide').show();
            }
        }, self.intervalFrecuency);
    };


    /**
     * Hide the previous playlist
     */
    self.hidePrevList = function(){
        for(var e =  0; e < self.currentIndex; e++){
            $('ul.timeline li').eq(e).fadeOut();
        }
    };

    /**
     * Move the video to specific player position
     * @param second
     */
    self.goTo = function(second){
        clearInterval(self.interval);

        self.videoReference.playbackRate = 1.0;
        self.videoReference.currentTime = second;

        self.videoReference.play();

        $('#pause', self.buttonsRef).removeClass('hide').show();
        $('#play', self.buttonsRef).removeClass('hide').hide();
    };

};