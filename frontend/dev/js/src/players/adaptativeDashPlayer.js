/** ---------------------------------------------------------------------------------------------------------------
 UI Module
 ---------------------------------------------------------------------------------------------------------------- **/
module.exports = function() {
    //++ Module
    var self = this;
    self.moduleName = "adaptativeDashPlayer";

    //++ Properties

    //++ Methods

    /**
     * Init
     */
    self.init = function(){
        var video_reference = $('<video />').appendTo($('.video-box'));
        var video = $('<source/>', {
            type     : 'application/x-mpegURL',
            controls : true
        });

        video.appendTo(video_reference);
        self.videoReference = video_reference[0];
        self.loadPlayer();
    };

    /**
     * Load the dash player
     */
    self.loadPlayer = function(){
        var player = dashjs.MediaPlayer().create();
        player.initialize(self.videoReference,'http://www.bok.net/dash/tears_of_steel/cleartext/stream.mpd',true); //url example
    };
};