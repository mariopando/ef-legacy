/**
 * BitDash View Model
 * @class bitDash
 */
module.exports = function() {

    //++ Module
    var self        = this;
    self.moduleName = "bitDash";

    //++ Properties
    self.player = null;

    /**
     * Init function
     */
    self.init = function( conf, id_element, callback) {

        //get bitdash script
        $.getScript("https://bitmovin-a.akamaihd.net/bitdash/latest/bitdash.min.js", function() {
            var player = bitdash(id_element);

            player.setup(conf).then(function(value) {
                self.player = value;
                //return value;
            }, function(reason) {
                console.log(reason);
                callback = false;
                //return false;
            });
        });
    };
};
