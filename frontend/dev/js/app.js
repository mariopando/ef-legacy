/**
 * App.js
 * Browserify can only analyze static requires.
 * It is not in the scope of browserify to handle dynamic requires.
 */

//load bundle dependencies
require('webpack_core');

//UI framework
require('foundation');

//motion UI (foundation)
require('motion-ui');

//Video JS
require('videojs');

// HLS Support
require('videojs-contrib-hls');

// DASH Support
require('dashjs-npm');

/* Load app modules */

var modules = [
    new (require('./src/ui.js'))(),
    new (require('./src/account.js'))(),
    new (require('./src/playerLoader.js'))(),
    new (require('./src/players/mpegPlayer.js'))(),
    new (require('./src/players/vimeoPlayer.js'))(),
    new (require('./src/players/adaptativeHlsPlayer.js'))(),
    new (require('./src/players/adaptativeDashPlayer.js'))(),
    new (require('./src/players/examPlayer.js'))()
];

//set modules
core.setModules(modules);

//Facebook module debugger: disabled js SDK
//core.modules.facebook.config.disable_js_sdk = false;
