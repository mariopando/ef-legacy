LiveOn Web App PHP Composer Libs
================================

Set Up
-------------

Install Composer
```
curl -sS https://getcomposer.org/installer | php
```

Update Composer
```
php composer.phar self-update
```

Usage
```
php composer.phar install
```

Update Libraries
```
php composer.phar update
```

Update One Vendor
```
php composer.phar update foo/bar
```

Add Libary
```
php composer.phar require "foo/bar:1.0.0"
```

Optimization loader for production
```
php composer.phar dump-autoload --optimize
```

Libraries
-------------
Read `composer.json` file
