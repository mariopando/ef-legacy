Engineering Football WebApp project
===================================

Requisitos de entorno
---------------------

- Apache 2.4.x or greater HTTP Server
- Phalcon PHP 2.x framework. [Get Phalcon](https://phalconphp.com/es/download).
- PHP 5.6.x
- PHP extensions: `curl`, `mcrypt`, `gd`, `gettext` and `mysql`
- MySQL 5.6.x server
- Composer (Library dependencies, `packages/composer` folder)
- Redis Server
- wkhtmltopdf (instalación abajo) [ref](http://wkhtmltopdf.org/)


Requisitos local development
----------------------------

- Para testear `cc-phalcon` crear *symLink* en directorio `packages/` apuntando al proyecto.
```ln -s ../../cc-phalcon packages/```

- nodeJs with global **(-g)** npm packages: `bower`, `gulp`, `browserify`, `uglifyjs`.
```bash _app.bash -npm-global```

Tasks
-----

Ejecutar **_app.bash** para ver commandos (alias bap)
```
bash _app.bash
```

Database Migrations
-------------------

Phinx: [documentation](https://phinx.org/)
```
bash _app.bash phinx <commands>
```

Redis Server
----------------------

Instalar server y cliente (testing) con
```sudo apt-get install redis-server redis-cli```


wkhtmltopdf (v 0.12.x)
----------------------

####Ubuntu Server
Instalar via shell:
```
bash _app.bash -wkhtmltopdf
```

finalmente, testear con:
```
/usr/local/bin/wkhtmltopdf.sh --lowquality http://www.google.com test.pdf
```

####OSX
Instalación en OSX
+ Bajar [binario](http://wkhtmltopdf.org/downloads.html)


Translations (GetText)
----------------------

Buscar nuevos translates en diferentes módulos
```
bash .tools/_translations.bash <module> -s
```

Compilar `.po` translation files en la carpeta `app` del módulo.
```
bash .tools/_translations.bash <module> -c
```

Para ver los 'locales' instalados en la instancia (getText)
```
locale -a
```

Composer
--------

Update Composer
```
php composer.phar self-update
```

Usage
```
php composer.phar install
```

Update Libraries
```
php composer.phar update
```

Update One Vendor
```
php composer.phar update foo/bar
```

Add Library
```
php composer.phar require "foo/bar:1.0.0"
```

Optimization loader for production
```
php composer.phar dump-autoload --optimize
```

Facebook
--------

Para test de login de Facebook en localhost, registrar *localhost.ef.cl* en nuestro archivo `/etc/hosts` y
acceder con dicha URL a nuestro localhost.
```
127.0.0.1	localhost localhost.engineeringfootball.com
```

###OpenGraph

[Graph Explorer](https://developers.facebook.com/tools/explorer)
[Object Browser](https://developers.facebook.com/tools/object-browser)

Ejemplo Graph Explorer
```
./1430373997281412 (object_id)
./730926267018069 (post_id)
```
