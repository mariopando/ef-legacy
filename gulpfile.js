/**
 *	Gulp App Builder Task Runner
 *	@author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//import libs
var browserify   = require('browserify');
var gulp         = require('gulp');
var source       = require('vinyl-source-stream');
var buffer       = require('vinyl-buffer');
var fs           = require("fs");
var watchify     = require('watchify');
var shell        = require('gulp-shell');
var argv         = require('yargs').argv;
var assign       = require('lodash.assign');
//plugins
var watch        = require('gulp-watch');
var gutil        = require('gulp-util');
var chmod        = require('gulp-chmod');
var rename       = require("gulp-rename");
var insert       = require('gulp-insert');
var autoprefixer = require('gulp-autoprefixer');
var sass         = require('gulp-sass');
var css_minifier = require('gulp-cssmin'); //CSS minifier
var livereload   = require('gulp-livereload');

/** Configurations **/

//webpack path
var webpack_name = 'webpack_core';
var webpack_path = './packages/webpacks/dist/js/'+webpack_name+'.bundle.js';

// sass paths
var sass_paths = [
    //motion-ui
    './node_modules/motion-ui/src/',
    //foundation sass files
    './node_modules/foundation-sites/scss/',
    //app common utils
    './packages/webpacks/dist/scss/',
    //bourbon path
    require('node-bourbon').includePaths,
    //video.js path
    './node_modules/video.js/src/css/'
];

//mailing ink file
var ink_css_file = "./packages/webpacks/private/ink/css/ink.css";

//set default app module
var app_module = argv.m;

if(typeof app_module == "undefined" || (app_module != "frontend" && app_module != "backend")) {
    gutil.log(gutil.colors.green('Invalid app module. Options: frontend or backend.'));
    return;
}
//log
gutil.log(gutil.colors.blue('Executing task for '+app_module+'...'));

//set assets paths
var assets_path    = './'+app_module+'/public/assets/';
var sass_src_files = './'+app_module+'/dev/scss/';
var volt_src_files = './'+app_module+'/dev/volt/';

// set up the browserify instance on a task basis
var opts = assign({}, watchify.args, {
    entries      : ['./'+app_module+'/dev/js/app.js'],
    cache        : {},
    packageCache : {}
});

/** Browserify setup **/
var b = watchify(browserify(opts)); //watchify
//options
b.external(webpack_name);   //external bundle with expose name
b.on('update', bundleApp);  //on any dep update, runs the bundler
b.on('log', gutil.log);     //output build logs to terminal

function bundleApp() {

    //browserify js bundler
    return b.bundle()
        .on('error', gutil.log.bind(gutil, 'Browserify Error'))
        .pipe(source('app.js'))
        .pipe(buffer())
        //prepend contents
        .pipe(insert.prepend(fs.readFileSync(webpack_path, "utf-8")))
        .pipe(gulp.dest(assets_path))
        .pipe(livereload());
}

/**  Gulp Tasks **/

//Watch tasks
gulp.task('watch', function() {

    gutil.log(gutil.colors.green('Watching scss or js source files changes...'));
    //live reaload
    livereload.listen();

    //js bundle
    bundleApp();

    //sass files
    gulp.watch(sass_src_files + "*.scss", ['compile-sass-app', 'compile-sass-mailing']);
    //volt files
    gulp.watch(volt_src_files + "**/*.volt", function() {

        return gulp.src(volt_src_files + 'index.volt').pipe(livereload());
    });
});

//Sass app compiler
gulp.task('compile-sass-app', function() {

    return gulp.src(sass_src_files + 'app.scss')
        .pipe(sass({ includePaths: sass_paths })
            .on('error', sass.logError))
        .pipe(autoprefixer({
            browsers : ['last 2 versions'],
            cascade  : false
        }))
        .pipe(gulp.dest(assets_path))
        .pipe(livereload());
});

//Sass mailing compiler
gulp.task('compile-sass-mailing', function() {

    return; //temporary
    return gulp.src(sass_src_files + 'mailing.scss')
        .pipe(sass({ includePaths: sass_paths })
            .on('error', sass.logError))
        .pipe(autoprefixer({
            browsers : ['last 2 versions'],
            cascade  : false
        }))
        //prepen ink css
        .pipe(insert.prepend(fs.readFileSync(ink_css_file, "utf-8")))
        .pipe(chmod(775))
        .pipe(gulp.dest(assets_path));
});

//CSS minifier
gulp.task('minify-css', function() {

    gutil.log(gutil.colors.yellow('Minifing css files...'));
    return gulp.src([assets_path + "*.css", "!" + assets_path + "*.*.css"])
        .pipe(buffer())
        .pipe(css_minifier())
        .pipe(rename({ suffix : ".min" }))
        .pipe(chmod(775))
        .pipe(gulp.dest(assets_path));
});


//JS minfier (gulp-uglify is slow)
gulp.task('minify-js', shell.task([
    "uglifyjs " + assets_path.replace("./","") + "app.js" + " -o " + assets_path.replace("./","") + "app.min.js"
]));

//Build & Deploy
gulp.task('build', ['minify-css', 'minify-js'], function() {

    //minification done
    gutil.log(gutil.colors.blue('Minification complete.'));
});
