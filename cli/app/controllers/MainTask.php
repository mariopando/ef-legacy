<?php
/**
 * CLI Main Task: main tasks for CLI commands
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//CrazyCake
use CrazyCake\Core\TaskCore;
use CrazyCake\Core\AppCore;

class MainTask extends TaskCore
{
    /**
     * Main Action Executer
     */
    public function mainAction()
    {
        parent::mainAction();
    }

    /**
     * User Tokens cleaner (for token checkout)
     */
    public function userTokenCleanerAction()
    {
        //delete tokens default expiration time
        $objs_deleted = UserToken::deleteExpired();

        //rows affected
        if($objs_deleted)
            echo "[".date("d-m-Y H:i:s")."] userTokenCleaner -> Expired Tokens deleted: ".$objs_deleted."\n";
    }
}
