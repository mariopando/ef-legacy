<?php
/**
 * Phalcon Project Environment configuration file.
 * Requires PhalconPHP installed
 * @author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//include CrazyCake phalcon loader
require is_link(__DIR__."/packages/cc-phalcon") ? __DIR__."/packages/cc-phalcon/autoload.php" : __DIR__."/packages/cc-phalcon.phar";

class PhalconApp extends \CrazyCake\Phalcon\AppLoader
{
    /**
     * Required app configuration
     */
    protected function config()
    {
        return [
            //Project Path
            "projectPath" => __DIR__."/",
            //Module Settings
            "modules" => [
                "frontend" => [
                    "loader"    => ["models", "helpers"],
                    "core"      => ["facebook"],
                    "langs"     => ["es", "en"],
                    "version"   => "0.0.5",
                ],
                "api" => [
                    "loader"     => [],
                    "core"       => [],
                    "version"    => "1.0",
                    "key"        => "*Cr4ZyCak3_EFx?", //HTTP header API Key (basic security)
                    "keyEnabled" => true
                ],
                "cli" => [
                    "loader" => ["models", "frontend/models"],
                    "core"   => []
                ]
            ],
            //App DI properties [$this->config->app->property]
            "app" => [
                //project properties
                "name"      => "Engineering Football",  //App name
                "namespace" => "ef",                    //App namespace (no usar underscore ni guiones)
                //crypto
                "cryptKey" => "CC4EF0x*",  //used for cypher
                //emails
                "emails" => [
                    "sender"  => "app@engineeringfootball.com",
                    "support" => "soporte@engineeringfootball.com",
                    "contact" => "contacto@engineeringfootball.com"
                ],
                //mandrill
                "mandrill" => [
                    "accessKey" => "RE3z78hfRD-qJm4EIlVnVw", //Mandrill API Key
                ],
                //amazon (Bucket is defined by environment)
                "aws" => [
                    "accessKey" => "AKIAI6K4UWDZMKGOKV4Q",                      //Access Key
                    "secretKey" => "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",  //Secret Key
                    "s3Bucket"  => "ef"                                         //s3 bucket prefix name
                ],
                //google
                "google" => [
                    "analyticsUA"  => "UA-71483743-1",                            //Analytics UA identifier (for frontend)
                    "reCaptchaID"  => "6LcPEgATAAAAAFgS7BYStybvZ841I7VWt80rjy-2", //reCaptcha ID (frontend & backend)
                    "reCaptchaKey" => "6LcPEgATAAAAANVUzYEvY_gTjW61LF4DhOIlJnyT", //reCaptcha Secret key (frontend & backend)
                ],
                //facebook
                "facebook" => [
                    "appScope" => "email,user_birthday",                 //app required permissions
                    "appID"    => "499832856845397",                     //app ID
                    "appKey"   => "702eb3e6aebd68bdc6e61ae9b7b1d60d",    //app Secret key
                ],
                //vimeo
                "vimeo" => [
                    "clientID"     => "b33970424b673ac44a8c75f7bf8dde21ead5a67a",
                    "clientSecret" => "7GrY66WiC2pt9dEzGFcMEbiaVMYFnMKKt/Xzexf8EdaUcFhsYrlrIQqw6c9gCoixbPZLq9NE2GGTRMzMok3u/ZHeCAai43YR1JT1+XwQfAaxhWxSx1J+cjlcBJDOi3x9",                     //app ID
                    "accessToken"  => "f8dd1a0e0ded017b69b5ad5c16b49190",
                ]
            ]
        ];
    }
}
